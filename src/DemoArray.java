package arraydemo;

import java.util.Random;
import java.util.Scanner;


public class DemoArray {
	
		public static int[] createRandomArray(int size,int range) {
			
			Random ran= new Random();
			Scanner sc = new Scanner(System.in);
			int[] ranArray = new int [size] ;
					for(int i=0; i<size; i++) {
						ranArray[i]=ran.nextInt(range);
						}
					printRandomArray(ranArray);
					return ranArray;
			}
			private static void printRandomArray(int[] array) {
				for (int element:array) {
					System.out.print(element +",");
				}
			}
	}
	
